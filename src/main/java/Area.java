/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Area {

    private int width;
    private int height;
    private Sweetcorn sweetcorn;
    private Carrot carrot;
    private Tomato tomato;
    private int water1 = 0;
    private int water2 = 0;
    private int water3 = 0;

    public Area(int width, int height) {
        this.height = height;
        this.width = width;
    }

    public void setSweetcorn(Sweetcorn sweetcorn) {
        this.sweetcorn = sweetcorn;
    }

    public void setCarrot(Carrot carrot) {
        this.carrot = carrot;
    }

    public void setTomato(Tomato tomato) {
        this.tomato = tomato;
    }

    public void showArea() {
        System.out.println("Area");
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (sweetcorn.isOn(x, y) || carrot.isOn(x, y) || tomato.isOn(x, y)) {
                    if (sweetcorn.isOn(x, y) && water1 >= 2) {
                        System.out.print("S");
                    } else if (carrot.isOn(x, y) && water2 >= 4) {
                        System.out.print("C");
                    } else if (tomato.isOn(x, y) && water3 >= 6) {
                        System.out.print("T");
                    } else {
                        System.out.print("x");
                    }

                } else {
                    System.out.print("x");
                }
            }
            System.out.println("");
        }
    }

    public void growTree(int x, int y) {
        if (sweetcorn.isOn(x, y)) {
            showArea();
            if (water1 < 2) {
                water1 += 1;
                System.out.println("The corn" + "(" + x + "," + y + ")" + " has been watered "
                        + water1 + " , the remaining " + (3 - water1) + " times.");
            } else {
                System.out.println("Corn has grown!!");
            }
        }
        if (carrot.isOn(x, y)) {
            showArea();
            if (water2 < 4) {
                water2 += 1;
                System.out.println("The Carrot" + "(" + x + "," + y + ")" + " has been watered "
                        + water2 + " , the remaining " + (5 - water2) + " times.");
            } else {
                System.out.println("Carrot has grown!!");
            }
        }
        if (tomato.isOn(x, y)) {
            showArea();
            if (water3 < 6) {
                water3 += 1;
                System.out.println("The Tomato" + "(" + x + "," + y + ")" + " has been watered "
                        + water3 + " , the remaining " + (7 - water3) + " times.");
            } else {
                System.out.println("Tomato has grown!!");
            }
        }
    }

    public void getWater1(int water1) {
        this.water1 = water1;
    }

    public void getWater2(int water2) {
        this.water2 = water2;
    }

    public void getWater3(int water3) {
        this.water3 = water3;
    }

    public void harvest() {
        System.out.println("harvest every tree!!!!");
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (water1 >= 2 && water2 >= 4 && water3 >= 6) {
                    System.out.print("x");
                }
            }
            System.out.println("");
        }
        water1 = 0;
        water2 = 0;
        water3 = 0;
        Object Area = null;
    }

    public void growTree(int x, int y, int water) {
        if (sweetcorn.isOn(x, y)) {
            showArea();
            if (water1 < 2) {
                water1 += water;
                System.out.println("The corn" + "(" + x + "," + y + ")" + " has been watered "
                        + water1 + " , the remaining " + (3 - water1) + " times.");
                System.out.println("You try watering again");
            } else {
                System.out.println("corn has grown!!");
            }
        }
        if (carrot.isOn(x, y)) {
            showArea();
            if (water2 < 4) {
                water2 += water;
                System.out.println("The carrot" + "(" + x + "," + y + ")" + " has been watered "
                        + water2 + " , the remaining " + (5 - water2) + " times.");
                System.out.println("You try watering again");
            } else {
                System.out.println("carrot has grown!!");
            }
        }
        if (tomato.isOn(x, y)) {
            showArea();
            if (water3 < 6) {
                water3 += water;
                System.out.println("The Tomato" + "(" + x + "," + y + ")" + " has been watered "
                        + water3 + " , the remaining " + (7 - water3) + " times.");
                System.out.println("You try watering again");
            } else {
                System.out.println("Tomato has grown!!");
            }
        }

    }

}
