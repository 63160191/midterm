/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Tree {

    protected int x;
    protected int y;

    public Tree(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void print() {
        System.out.println("Tree created");
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}
